/**********************************************************************
Copyright �2013 Advanced Micro Devices, Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

�	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
�	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************/

/*
 * For a description of the algorithm and the terms used, please see the
 * documentation for this sample.
 *
 * Each thread calculates a pixel component(rgba), by applying a filter 
 * on group of 8 neighbouring pixels in both x and y directions. 
 * Both filters are summed (vector sum) to form the final result.
 */

#include "host_kern_head.hpp"

__kernel void sobel_filter( __const __global uchar4* inputImage, __global uchar4* outputImage,
                             __const __global uchar* table, const unsigned int N )
{
	uint x_d = get_global_id(0);
    uint y_d = get_global_id(1);

	uint width = get_global_size(0);
	uint height = get_global_size(1);
	
	int c = x_d + y_d * width;
    uchar4 input_tuple = inputImage[c];
    int4 yuv = convert_int4_sat( input_tuple / STRIDE );
    int4 P3;
    
    P3  = convert_int4_sat( input_tuple % STRIDE );    //  v u y
    
	uint index =  (yuv.s0*N*N+yuv.s1*N+yuv.s2)*ROFFS;
	
    int4  output_tuple;
    __global uchar4* p4_table;
    
    p4_table = (__global uchar4*)(table + index);
    output_tuple = convert_int4_sat(p4_table[0]);

    int4 vuy_v, vuy_u, vuy_y; 
    vuy_v.s0 =as_char(*(table+index+3));      vuy_v.s1 =as_char(*(table+index+4));   vuy_v.s2 =as_char(*(table+index+5));
    vuy_u.s0 =as_char(*(table+index+6));      vuy_u.s1 =as_char(*(table+index+7));   vuy_u.s2 =as_char(*(table+index+8));
    vuy_y.s0 =as_char(*(table+index+9));      vuy_y.s1 =as_char(*(table+index+10));  vuy_y.s2 =as_char(*(table+index+11));

    output_tuple = output_tuple + (P3.s0*vuy_v+P3.s1*vuy_u+P3.s2*vuy_y)/STRIDE;

    
    output_tuple.s3 = input_tuple.s3;
	outputImage[c] = convert_uchar4_sat(output_tuple);		
	//outputImage[c] = (uchar4)N;	
	//outputImage[c] = (uchar4)Table[0];				
}


__kernel void sobel_filterYuv420( __const __global uchar* inputImage, __global uchar* outputImage,
                             __const __global uchar* table, const unsigned int N )
{

    uint x_d = get_global_id(0);
    uint y_d = get_global_id(1);

    if(x_d % 2 == 1  || y_d%2 == 1)
        return;

    uint width = get_global_size(0);
    uint height = get_global_size(1);

    uint c = x_d + y_d * width;


    uint  u_start = width*height;
    uint  v_start = u_start + width*height/4;

    uint cu = u_start +  x_d/2 + y_d/2 * width/2;
    uint cv = v_start +  x_d/2 + y_d/2 * width/2;  

    int  y0 = inputImage[c];
    int  y1 = inputImage[c+1];   
    int  y2 = inputImage[c+width];     
    int  y3 = inputImage[c+width+1]; 
    int LL = (y0+y1+y2+y3)/4;

    uchar4 input_tuple ;
    input_tuple.s0 =  inputImage[cv]; input_tuple.s1 = inputImage[cu]; input_tuple.s2 = LL;

    int4 yuv = convert_int4_sat( input_tuple / STRIDE );
    int4 P3;  
    
    P3  = convert_int4_sat( input_tuple % STRIDE );    // v u y
    
	uint index =  (yuv.s0*N*N+yuv.s1*N+yuv.s2)*ROFFS;


    int4  output_tuple;
    __global uchar4* p4_table;
    
    p4_table = (__global uchar4*)(table + index);
    output_tuple = convert_int4_sat(p4_table[0]);

    int4 vuy_v, vuy_u, vuy_y; 


    vuy_v.s0 =as_char(*(table+index+3));      vuy_v.s1 =as_char(*(table+index+4));   vuy_v.s2 =as_char(*(table+index+5));
    vuy_u.s0 =as_char(*(table+index+6));      vuy_u.s1 =as_char(*(table+index+7));   vuy_u.s2 =as_char(*(table+index+8));
    vuy_y.s0 =as_char(*(table+index+9));      vuy_y.s1 =as_char(*(table+index+10));  vuy_y.s2 =as_char(*(table+index+11));

    output_tuple = output_tuple + (P3.s0*vuy_v+P3.s1*vuy_u+P3.s2*vuy_y)/STRIDE;

    int diff = output_tuple.s2 - LL; 

    y0 += diff;
    y1 += diff;   
    y2 += diff;     
    y3 += diff; 
    
    outputImage[cv]=output_tuple.s0;   
    outputImage[cu] = output_tuple.s1;
    
    outputImage[c]=y0;
    outputImage[c+1]=y1;   
    outputImage[c+width]=y2;     
    outputImage[c+width+1]=y3; 

}


	

	 






	

	




	

	

	
	
