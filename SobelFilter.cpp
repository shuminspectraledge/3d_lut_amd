/**********************************************************************
Copyright �2013 Advanced Micro Devices, Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

�	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
�	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************/


#include "SobelFilter.hpp"
#include <cmath>
#include <string>


int
SobelFilter::readInputImage(std::string inputImageName)
{

    // load input bitmap image 
    inputBitmap.load(inputImageName.c_str());

    // error if image did not load
    if(!inputBitmap.isLoaded())
    {
        std::cout << "Failed to load input image!";
        return SDK_FAILURE;
    }


    // get width and height of input image 
    height = inputBitmap.getHeight();
    width = inputBitmap.getWidth();

    // allocate memory for input & output image data 
    inputImageData  = (cl_uchar4*)malloc(width * height * sizeof(cl_uchar4));
    CHECK_ALLOCATION(inputImageData, "Failed to allocate memory! (inputImageData)");

    // allocate memory for output image data 
    outputImageData = (cl_uchar4*)malloc(width * height * sizeof(cl_uchar4));
    CHECK_ALLOCATION(outputImageData, "Failed to allocate memory! (outputImageData)");

    outputImageDataCpu= (cl_uchar4*)malloc(width * height * sizeof(cl_uchar4));
    CHECK_ALLOCATION(outputImageDataCpu, "Failed to allocate memory! (outputImageDataCpu)");   

    // initializa the Image data to NULL
    memset(outputImageData, 0, width * height * pixelSize);
    memset(outputImageDataCpu, 0, width * height * pixelSize);

    

    // get the pointer to pixel data 
    pixelData = inputBitmap.getPixels();
    if(pixelData == NULL)
    {
        std::cout << "Failed to read pixel Data!";
        return SDK_FAILURE;
    }

    // Copy pixel data into inputImageData 
    memcpy(inputImageData, pixelData, width * height * pixelSize);

    // allocate memory for verification output
    verificationOutput = (cl_uchar*)malloc(width * height * pixelSize);
    CHECK_ALLOCATION(verificationOutput, "verificationOutput heap allocation failed!");

    // initialize the data to NULL 
    memset(verificationOutput, 0, width * height * pixelSize);

    return SDK_SUCCESS;

}


int
SobelFilter::writeOutputImage(std::string outputImageName)
{
    // copy output image data back to original pixel data
    memcpy(pixelData, outputImageData, width * height * pixelSize);

    // write the output bmp file
    if(!inputBitmap.write(outputImageName.c_str()))
    {
        std::cout << "Failed to write output image!";
        return SDK_FAILURE;
    }

    return SDK_SUCCESS;
}

int 
SobelFilter::genBinaryImage()
{
    streamsdk::bifData binaryData;
    binaryData.kernelName = std::string("SobelFilter_Kernels.cl");
    binaryData.flagsStr = std::string("");
    if(isComplierFlagsSpecified())
        binaryData.flagsFileName = std::string(flags.c_str());

    binaryData.binaryName = std::string(dumpBinary.c_str());
    int status = sampleCommon->generateBinaryImage(binaryData);
    return status;
}



int 
SobelFilter::setupCL()
{
    cl_int status = CL_SUCCESS;
    cl_device_type dType;

    if(deviceType.compare("cpu") == 0)
    {
        dType = CL_DEVICE_TYPE_CPU;
    }
    else //deviceType = "gpu" 
    {
        dType = CL_DEVICE_TYPE_GPU;
        if(isThereGPU() == false)
        {
            std::cout << "GPU not found. Falling back to CPU device" << std::endl;
            dType = CL_DEVICE_TYPE_CPU;
        }
    }

    /*
     * Have a look at the available platforms and pick either
     * the AMD one if available or a reasonable default.
     */
    cl_platform_id platform = NULL;
    int retValue = sampleCommon->getPlatform(platform, platformId, isPlatformEnabled());
    CHECK_ERROR(retValue, SDK_SUCCESS, "sampleCommon::getPlatform() failed");

    // Display available devices.
    retValue = sampleCommon->displayDevices(platform, dType);
    CHECK_ERROR(retValue, SDK_SUCCESS, "sampleCommon::displayDevices() failed");


    // If we could find our platform, use it. Otherwise use just available platform.
    cl_context_properties cps[3] = 
    {
        CL_CONTEXT_PLATFORM, 
        (cl_context_properties)platform, 
        0
    };

    context = clCreateContextFromType(
                  cps,
                  dType,
                  NULL,
                  NULL,
                  &status);
    CHECK_OPENCL_ERROR( status, "clCreateContextFromType failed.");

    // getting device on which to run the sample
    status = sampleCommon->getDevices(context,&devices,deviceId,isDeviceIdEnabled());
    CHECK_ERROR(status, SDK_SUCCESS, "sampleCommon::getDevices() failed");

    {
        // The block is to move the declaration of prop closer to its use
        cl_command_queue_properties prop = 0;
        commandQueue = clCreateCommandQueue(
                context, 
                devices[deviceId], 
                prop, 
                &status);
        CHECK_OPENCL_ERROR( status, "clCreateCommandQueue failed.");
    }

    //Set device info of given cl_device_id
    retValue = deviceInfo.setDeviceInfo(devices[deviceId]);
    CHECK_ERROR(retValue, 0, "SDKDeviceInfo::setDeviceInfo() failed");


    // Create and initialize memory objects

    // Set Presistent memory only for AMD platform
    cl_mem_flags inMemFlags = CL_MEM_READ_ONLY;
    if(isAmdPlatform())
        inMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD;

    // Create memory object for input Image
    inputImageBuffer = clCreateBuffer(
        context,
        inMemFlags,
        width * height * pixelSize,
        0,
        &status);
    CHECK_OPENCL_ERROR(status, "clCreateBuffer failed. (inputImageBuffer)");

    // Create memory objects for output Image
    outputImageBuffer = clCreateBuffer(context,
        CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
        width * height * pixelSize,
        outputImageData,
        &status);
    CHECK_OPENCL_ERROR(status, "clCreateBuffer failed. (outputImageBuffer)");


    // Create memory objects for 3D lookup table 
    
    _3dTableBuffer = clCreateBuffer(
        context,
        inMemFlags,
        n_table * n_table * n_table* entrySize,
        0,
        &status);
    CHECK_OPENCL_ERROR(status, "clCreateBuffer failed. (3D lookup table)");

    // create a CL program using the kernel source 
    streamsdk::buildProgramData buildData;
    buildData.kernelName = std::string("SobelFilter_Kernels.cl");
    buildData.devices = devices;
    buildData.deviceId = deviceId;
    buildData.flagsStr = std::string("-I ./");
    if(isLoadBinaryEnabled())
        buildData.binaryName = std::string(loadBinary.c_str());

    if(isComplierFlagsSpecified())
        buildData.flagsFileName = std::string(flags.c_str());

    retValue = sampleCommon->buildOpenCLProgram(program, context, buildData);
    CHECK_ERROR(retValue, 0, "sampleCommon::buildOpenCLProgram() failed");

    // get a kernel object handle for a kernel with the given name
    if( inputFile_t == BMP)
    {
        kernel = clCreateKernel(
            program,
            "sobel_filter",
            &status);
    }
    else if ( inputFile_t == YUV )
    {
        kernel = clCreateKernel(
            program,
            "sobel_filterYuv420",
            &status);
    }
    else
    {
            return SDK_FAILURE;
    }
        
    CHECK_OPENCL_ERROR(status, "clCreateKernel failed.");

    status = kernelInfo.setKernelWorkGroupInfo(kernel,devices[deviceId]);
    CHECK_ERROR(status, SDK_SUCCESS,"kernelInfo.setKernelWorkGroupInfo() failed");


    if((blockSizeX * blockSizeY) > kernelInfo.kernelWorkGroupSize)
    {
        if(!quiet)
        {
            std::cout << "Out of Resources!" << std::endl;
            std::cout << "Group Size specified : "
                      << blockSizeX * blockSizeY << std::endl;
            std::cout << "Max Group Size supported on the kernel : " 
                      << kernelWorkGroupSize << std::endl;
            std::cout << "Falling back to " << kernelInfo.kernelWorkGroupSize << std::endl;
        }

        // Three possible cases
        if(blockSizeX > kernelInfo.kernelWorkGroupSize)
        {
            blockSizeX = kernelInfo.kernelWorkGroupSize;
            blockSizeY = 1;
        }
    }
    return SDK_SUCCESS;
}

int 
SobelFilter::runCLKernels()
{
    cl_int status;
    clock_t start = clock();
    
    // Set input data 
    cl_event writeEvt;
    status = clEnqueueWriteBuffer(
                commandQueue,
                inputImageBuffer,
                CL_FALSE,
                0,
                width * height * pixelSize,
                inputImageData,
                0,
                NULL,
                &writeEvt);
    CHECK_OPENCL_ERROR(status, "clEnqueueWriteBuffer failed. (inputImageBuffer)");

    status = clFlush(commandQueue);
    CHECK_OPENCL_ERROR(status, "clFlush failed.");

    status = sampleCommon->waitForEventAndRelease(&writeEvt);
    CHECK_ERROR(status, SDK_SUCCESS, "WaitForEventAndRelease(writeEvt) Failed");


    //Set input 3D table
    status = clEnqueueWriteBuffer(
                commandQueue,
                _3dTableBuffer,
                CL_FALSE,
                0,
                n_table*n_table*n_table*entrySize,
                TableData,
                0,
                NULL,
                &writeEvt);
    CHECK_OPENCL_ERROR(status, "clEnqueueWriteBuffer failed. (3D lookup table input Buffer)");

    status = clFlush(commandQueue);
    CHECK_OPENCL_ERROR(status, "clFlush failed.");

    status = sampleCommon->waitForEventAndRelease(&writeEvt);
    CHECK_ERROR(status, SDK_SUCCESS, "WaitForEventAndRelease(writeEvt) Failed");




    // Set appropriate arguments to the kernel 

    // input buffer image , first argument
    status = clSetKernelArg(
        kernel,
        0,
        sizeof(cl_mem),
        &inputImageBuffer);
    CHECK_OPENCL_ERROR(status, "clSetKernelArg 0 failed. (inputImageBuffer)")

    // outBuffer imager, second argument
    status = clSetKernelArg(
        kernel,
        1,
        sizeof(cl_mem),
        &outputImageBuffer);
    CHECK_OPENCL_ERROR(status, "clSetKernelArg 1 failed. (outputImageBuffer)");

    // 3d lookup table input , third argument
    status = clSetKernelArg(
        kernel,
        2,
        sizeof(cl_mem),
        &_3dTableBuffer);
    CHECK_OPENCL_ERROR(status, "clSetKernelArg 2 failed. (3D look up Buffer)");

    //  the length of table in each demension, fourth argument
    status = clSetKernelArg(
        kernel, 
        3, 
        sizeof(cl_uint), 
        (void *)&n_table );
    CHECK_OPENCL_ERROR(status, "clSetKernelArg 3 failed. ( n_table) ");


    // Enqueue a kernel run call.
    size_t globalThreads[] = {width, height};
    size_t localThreads[] = {blockSizeX, blockSizeY};

    
    cl_event ndrEvt;
    status = clEnqueueNDRangeKernel(
        commandQueue,
        kernel,
        2,
        NULL,
        globalThreads,
        localThreads,
        0,
        NULL,
        &ndrEvt);
    CHECK_OPENCL_ERROR(status, "clEnqueueNDRangeKernel failed.");

    status = clFlush(commandQueue);
    CHECK_OPENCL_ERROR(status, "clFlush failed.");

    status = sampleCommon->waitForEventAndRelease(&ndrEvt);
    CHECK_ERROR(status, SDK_SUCCESS, "WaitForEventAndRelease(ndrEvt) Failed");


    // Enqueue readBuffer
    cl_event readEvt;
    status = clEnqueueReadBuffer(
        commandQueue,
        outputImageBuffer,
        CL_FALSE,
        0,
        width * height * pixelSize,
        outputImageData,
        0,
        NULL,
        &readEvt);
    CHECK_OPENCL_ERROR(status, "clEnqueueReadBuffer failed.");

    status = clFlush(commandQueue);
    CHECK_OPENCL_ERROR(status, "clFlush failed.");

    status = sampleCommon->waitForEventAndRelease(&readEvt);
    CHECK_ERROR(status, SDK_SUCCESS, "WaitForEventAndRelease(readEvt) Failed");

    clock_t  finish = clock();
    double duration = (double)(finish - start) / CLOCKS_PER_SEC;
    std::cout<<"width:"<<width<<" height:"<<height<<" time for one frame:"<< duration*1000 << " milli sencod" << std::endl;

    return SDK_SUCCESS;
}



int 
SobelFilter::initialize()
{
    cl_int status = 0;
    // Call base class Initialize to get default configuration
    status = this->SDKSample::initialize();
    CHECK_ERROR(status, SDK_SUCCESS, "OpenCL Initialization failed");

    streamsdk::Option* iteration_option = new streamsdk::Option;
    CHECK_ALLOCATION(iteration_option, "Memory Allocation error.\n");

    iteration_option->_sVersion = "i";
    iteration_option->_lVersion = "iterations";
    iteration_option->_description = "Number of iterations to execute kernel";
    iteration_option->_type = streamsdk::CA_ARG_INT;
    iteration_option->_value = &iterations;

    sampleArgs->AddOption(iteration_option);
    
    delete iteration_option;

    entrySize = ROFFS;
    table_File.open("derivLUT1.txt", std::ifstream::in);
    table_File >> n_table;

    // allocate memory for 3d lookup table
    TableData = (cl_uchar *)malloc( n_table*n_table*n_table*entrySize);
    CHECK_ALLOCATION(TableData, "Failed to allocate memory! (3dTableData)");

    int table_value; //  operator >> is not overload for istream, errors, http://www.cplusplus.com/reference/istream/istream/operator%3E%3E/
    for ( unsigned int i=0; i< n_table*n_table*n_table*entrySize; i++ )
    {
        table_File >> table_value;
        TableData[i] = table_value;    
        if( i%ROFFS == 0 )
        {
            //printf("\n"); 
        }        
        //printf("%d ", TableData[i] );

    }
    
    return SDK_SUCCESS;
}

int 
SobelFilter::setup()
{
    cl_int status = 0;
    // Allocate host memory and read input image 
    std::string filePath = sampleCommon->getPath() + std::string(INPUT_IMAGE);

	if(filePath.find(".bmp")!= std::string::npos)
	{
	    inputFile_t = BMP;
        status = readInputImage(filePath);
        
    }
    else if(filePath.find(".yuv")!= std::string::npos)
    {
        status = readInputYuvImage(filePath);
        inputFile_t = YUV;
    }
    else
        return SDK_FAILURE;
        
    CHECK_ERROR(status, SDK_SUCCESS, "Read InputImage failed");

    // create and initialize timers
    int timer = sampleCommon->createTimer();
    sampleCommon->resetTimer(timer);
    sampleCommon->startTimer(timer);

    status = setupCL();
    if(status != SDK_SUCCESS)
        return status;

    sampleCommon->stopTimer(timer);
    // Compute setup time
    setupTime = (double)(sampleCommon->readTimer(timer));

    return SDK_SUCCESS;
}


int 
SobelFilter::run()
{
    cl_int status = 0;
    if(!byteRWSupport)
        return SDK_SUCCESS;

    for(int i = 0; i < 2 && iterations != 1; i++)
    {
        // Set kernel arguments and run kernel
        if(runCLKernels() != SDK_SUCCESS)
            return SDK_FAILURE;
    }

    std::cout << "Executing kernel for " << iterations
              << " iterations" <<std::endl;
    std::cout << "-------------------------------------------" << std::endl;

    // create and initialize timers 
    int timer = sampleCommon->createTimer();
    sampleCommon->resetTimer(timer);
    sampleCommon->startTimer(timer);

    //if ( inputFile_t == BMP )
    {
        for(int i = 0; i < iterations; i++)
        {
            // Set kernel arguments and run kernel
            if(runCLKernels() != SDK_SUCCESS)
            return SDK_FAILURE;
        }
    }

    sampleCommon->stopTimer(timer);
    // Compute kernel time
    kernelTime = (double)(sampleCommon->readTimer(timer)) / iterations;

    // write the output image to bitmap file

    if ( inputFile_t == BMP )
        status = writeOutputImage(OUTPUT_IMAGE);
    else
        status = writeOutputYuvImage(OUTPUT_IMAGE);
        
    CHECK_ERROR(status, SDK_SUCCESS, "write Output Image Failed");

    return SDK_SUCCESS;
}

int 
SobelFilter::cleanup()
{
    if(!byteRWSupport)
        return SDK_SUCCESS;

    // Releases OpenCL resources (Context, Memory etc.)
    cl_int status;

    status = clReleaseKernel(kernel);
    CHECK_OPENCL_ERROR(status, "clReleaseKernel failed.");

    status = clReleaseProgram(program);
    CHECK_OPENCL_ERROR(status, "clReleaseProgram failed.");

    status = clReleaseMemObject(inputImageBuffer);
    CHECK_OPENCL_ERROR(status, "clReleaseMemObject failed.");

    status = clReleaseMemObject(outputImageBuffer);
    CHECK_OPENCL_ERROR(status, "clReleaseMemObject failed.");

    status = clReleaseMemObject( _3dTableBuffer );
    CHECK_OPENCL_ERROR(status, "clReleaseMemObject failed.");	

    status = clReleaseCommandQueue(commandQueue);
    CHECK_OPENCL_ERROR(status, "clReleaseCommandQueue failed.");

    status = clReleaseContext(context);
    CHECK_OPENCL_ERROR(status, "clReleaseContext failed.");

    table_File.close();

    // release program resources (input memory etc.)
    FREE(inputImageData);

    FREE(outputImageData);
    
    FREE(outputImageDataCpu);   

    FREE(verificationOutput);

    FREE(devices);

    FREE(TableData);

    return SDK_SUCCESS;
}


void 
SobelFilter::sobelFilterCPUReference()
{
    // x-axis gradient mask 
    const int kx[][3] = 
    { 
        { 1, 2, 1},
        { 0, 0, 0},
        { -1,-2,-1}
    };

    // y-axis gradient mask
    const int ky[][3] = 
    { 
        { 1, 0, -1},
        { 2, 0, -2},
        { 1, 0, -1}
    };

    int gx = 0;
    int gy = 0;

    // pointer to input image data
    cl_uchar *ptr = (cl_uchar*)malloc(width * height * pixelSize);
    memcpy(ptr, inputImageData, width * height * pixelSize);

    // each pixel has 4 uchar components 
    int w = width * 4;

    int k = 1;

    // apply filter on each pixel (except boundary pixels)
    for(int i = 0; i < (int)(w * (height - 1)) ; i++) 
    {
        if(i < (k+1)*w - 4 && i >= 4 + k*w)
        {
            gx =  kx[0][0] * *(ptr + i - 4 - w)
                + kx[0][1] * *(ptr + i - w)
                + kx[0][2] * *(ptr + i + 4 - w)
                + kx[1][0] * *(ptr + i - 4)
                + kx[1][1] * *(ptr + i)
                + kx[1][2] * *(ptr + i + 4)
                + kx[2][0] * *(ptr + i - 4 + w)
                + kx[2][1] * *(ptr + i + w)
                + kx[2][2] * *(ptr + i + 4 + w);


            gy =  ky[0][0] * *(ptr + i - 4 - w)
                + ky[0][1] * *(ptr + i - w)
                + ky[0][2] * *(ptr + i + 4 - w)
                + ky[1][0] * *(ptr + i - 4)
                + ky[1][1] * *(ptr + i)
                + ky[1][2] * *(ptr + i + 4)
                + ky[2][0] * *(ptr + i - 4 + w)
                + ky[2][1] * *(ptr + i + w)
                + ky[2][2] * *(ptr + i + 4 + w);

            float gx2 = pow((float)gx, 2);
            float gy2 = pow((float)gy, 2);


            *(verificationOutput + i) = (cl_uchar)(sqrt(gx2 + gy2) / 2.0);
        }

        // if reached at the end of its row then incr k
        if(i == (k + 1) * w - 5)
        {
            k++;
        }
    } 

    free(ptr);
}


int 
SobelFilter::verifyResults()
{
    if(!byteRWSupport)
        return SDK_SUCCESS;

    if(verify)
    {
        // reference implementation
        sobelFilterCPUReference();

        float *outputDevice = new float[width * height * pixelSize];
        CHECK_ALLOCATION(outputDevice, "Failed to allocate host memory! (outputDevice)");

        float *outputReference = new float[width * height * pixelSize];
        CHECK_ALLOCATION(outputReference, "Failed to allocate host memory!"
                "(outputReference)");

        // copy uchar data to float array
        for(int i = 0; i < (int)(width * height); i++)
        {
            outputDevice[i * 4 + 0] = outputImageData[i].s[0];
            outputDevice[i * 4 + 1] = outputImageData[i].s[1];
            outputDevice[i * 4 + 2] = outputImageData[i].s[2];
            outputDevice[i * 4 + 3] = outputImageData[i].s[3];

            outputReference[i * 4 + 0] = verificationOutput[i * 4 + 0];
            outputReference[i * 4 + 1] = verificationOutput[i * 4 + 1];
            outputReference[i * 4 + 2] = verificationOutput[i * 4 + 2];
            outputReference[i * 4 + 3] = verificationOutput[i * 4 + 3];
        }


        // compare the results and see if they match
        if(sampleCommon->compare(outputReference, 
                                 outputDevice, 
                                 width * height * 4))
        {
            std::cout << "Passed!\n" << std::endl;
            delete[] outputDevice;
            delete[] outputReference;
            return SDK_SUCCESS;
        }
        else
        {
            std::cout << "Failed\n" << std::endl;
            delete[] outputDevice;
            delete[] outputReference;
            return SDK_FAILURE;
        }
    }

    return SDK_SUCCESS;
}

int 
SobelFilter:: lookupCpu ( cl_uchar4 * inputImage,  cl_uchar4* outputImage, 
                            cl_uchar* table, cl_uint N )
{
    cl_uint index;   
    for ( cl_uint i =0; i<width;i++)
        for (cl_uint j=0; j<height; j++ )
        {

            int LL, LLo;
            cl_uchar u,v, uout, vout ;        
            int y0, u0, v0;
            int lutind[8];
            int P[3];

            index = (i*width + j);
            
            LL =  inputImage[index].s[0];
            u  =  inputImage[index].s[1];
            v  =  inputImage[index].s[2];
            
            y0 = LL / STRIDE;
            u0 = u / STRIDE;
            v0 = v / STRIDE;

            lutind[0] = ROFFS*(v0*N*N + u0*N + y0); 
            P[0] = LL % STRIDE;
            P[1] = u % STRIDE;
            P[2] = v % STRIDE; 

            int  v_floor, u_floor, y_floor;
            int  vv, vu, vy, uv, uu, uy, yv,yu,yy;

            //int lat_idx = 0;


            v_floor =  *(signed char *) &table[lutind[(0)]+0];
            u_floor =  *(signed char *) &table[lutind[(0)]+1];
            y_floor =  *(signed char *) &table[lutind[(0)]+2];
            vv =  *(signed char *) &table[lutind[(0)]+3];
            vu =  *(signed char *) &table[lutind[(0)]+4];
            vy =  *(signed char *) &table[lutind[(0)]+5];
            uv =  *(signed char *) &table[lutind[(0)]+6];
            uu =  *(signed char *) &table[lutind[(0)]+7];
            uy =  *(signed char *) &table[lutind[(0)]+8];
            yv =  *(signed char *) &table[lutind[(0)]+9];
            yu =  *(signed char *) &table[lutind[(0)]+10];
            yy =  *(signed char *) &table[lutind[(0)]+11];

            
            v_floor = v_floor + (P[2]*vv + P[1]*vu + P[0]*vy)/STRIDE;
            u_floor = u_floor + (P[2]*uv + P[1]*uu + P[0]*uy)/STRIDE;
            y_floor = y_floor + (P[2]*yv + P[1]*yu + P[0]*yy)/STRIDE;
            
            LLo = y_floor;
            uout = u_floor;
            vout = v_floor;

            outputImage[index].s[0]  = LLo;
            outputImage[index].s[1]  = uout;
            outputImage[index].s[2]  = vout;          
             
        }
  
        for ( cl_uint i =0; i<width;i++)
            for (cl_uint j=0; j<height; j++ )
            {
                index = (i*width + j);
                if( outputImageData[index].s[0] != outputImageDataCpu[index].s[0] )
                {
                    return SDK_FAILURE;
                }
                if( outputImageData[index].s[1] != outputImageDataCpu[index].s[1] )
                {
                    return SDK_FAILURE;
                } 
                if( outputImageData[index].s[2] != outputImageDataCpu[index].s[2] )
                {
                    return SDK_FAILURE;
                }             
            }     

        return SDK_SUCCESS;            
}


int 
SobelFilter::verifyGPU()
{

    if( inputFile_t = YUV )
        return SDK_SUCCESS;       

    return lookupCpu ( inputImageData, outputImageDataCpu, TableData, n_table);

}

void 
SobelFilter::printStats()
{
    std::string strArray[4] = 
    {
        "Width", 
        "Height", 
        "Time(sec)", 
        "[Transfer+Kernel]Time(sec)"
    };
    std::string stats[4];

    totalTime = setupTime + kernelTime;

    stats[0] = sampleCommon->toString(width, std::dec);
    stats[1] = sampleCommon->toString(height, std::dec);
    stats[2] = sampleCommon->toString(totalTime, std::dec);
    stats[3] = sampleCommon->toString(kernelTime, std::dec);

    this->SDKSample::printStats(strArray, stats, 4);
}


int 
main(int argc, char * argv[])
{
    cl_int status = 0;
    SobelFilter clSobelFilter("OpenCL SobelFilter");

    if(clSobelFilter.initialize() != SDK_SUCCESS)
        return SDK_FAILURE;

    if(clSobelFilter.parseCommandLine(argc, argv) != SDK_SUCCESS)
        return SDK_FAILURE;

    if(clSobelFilter.isDumpBinaryEnabled())
    {
        return clSobelFilter.genBinaryImage();
    }

    status = clSobelFilter.setup();
    if(status != SDK_SUCCESS)
        return (status == SDK_EXPECTED_FAILURE) ? SDK_SUCCESS : SDK_FAILURE;

    if(clSobelFilter.run() != SDK_SUCCESS)
        return SDK_FAILURE;

    if(clSobelFilter.verifyResults() != SDK_SUCCESS)
        return SDK_FAILURE;

    if(clSobelFilter.verifyGPU() != SDK_SUCCESS)
        return SDK_FAILURE;
    
    if(clSobelFilter.cleanup() != SDK_SUCCESS)
        return SDK_FAILURE;

    clSobelFilter.printStats();
    return SDK_SUCCESS;
}
