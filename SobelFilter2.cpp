/**********************************************************************
Copyright �2013 Advanced Micro Devices, Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

�	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
�	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************/


#include "SobelFilter.hpp"
#include <cmath>
#include <string>
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream


int  SobelFilter::readInputYuvImage(std::string inputImageName)
{

    // get width and height of input image 
    height = HEIGHT;
    width =  WIDTH;
   
    // allocate memory for input & output image data 
    inputImageData  = (cl_uchar4 *)malloc(width * height * sizeof(cl_uchar4));
    CHECK_ALLOCATION(inputImageData, "Failed to allocate memory! (inputImageData)");

    // allocate memory for output image data 
    outputImageData = (cl_uchar4*)malloc(width * height * sizeof(cl_uchar4));
    CHECK_ALLOCATION(outputImageData, "Failed to allocate memory! (outputImageData)");

    outputImageDataCpu= (cl_uchar4*)malloc(width * height * sizeof(cl_uchar4));
    CHECK_ALLOCATION(outputImageDataCpu, "Failed to allocate memory! (outputImageDataCpu)");   

    // initializa the Image data to NULL
    memset(outputImageData, 0, width * height * pixelSize);
    memset(outputImageDataCpu, 0, width * height * pixelSize);


	std::ifstream  inputFile;
	inputFile.open(INPUT_IMAGE, std::ios::binary );

    char temp;
    char* input_p= ( char*) inputImageData;

	if( inputFile.is_open() )
	{
    	int i;
    	for (i=0; i< HEIGHT*WIDTH + HEIGHT*WIDTH/4 + HEIGHT*WIDTH/4; i++ )
    	{
			inputFile.get( temp );
			input_p[i] = temp;    	
    	}	
	}
	else
	{
		printf(" input file open failure \n");
	}

	inputFile.close();

    // allocate memory for verification output
    verificationOutput = (cl_uchar*)malloc(width * height * pixelSize);
    CHECK_ALLOCATION(verificationOutput, "verificationOutput heap allocation failed!");

    // initialize the data to NULL 
    memset(verificationOutput, 0, width * height * pixelSize);

    return SDK_SUCCESS;

}

int SobelFilter::writeOutputYuvImage(std::string outputImageName)
{

	std::ofstream  outputFile;
	outputFile.open( OUTPUT_IMAGE, std::ios::binary );

    char temp;
    char* output_p= ( char*) outputImageData;

	if( outputFile.is_open() )
	{
    	int i;
    	for (i=0; i< HEIGHT*WIDTH + HEIGHT*WIDTH/4 + HEIGHT*WIDTH/4; i++ )
    	{
			temp = output_p[i]; 
			outputFile.put(temp);
    	}	
	}
	else
	{
		printf(" output file open failure \n");
	}

	outputFile.close();
    return SDK_SUCCESS;
}

